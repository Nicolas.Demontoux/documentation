# Good things for VisualStudioCode :

### The extensions :
- Doxygen Documentation Generator
- Git Graph
- JS-CSS-HTML Formatter
- Markdown Preview Enhanced

### Important shortcuts :
Display the cmd :
```
ALT + ù
```

Formate text :
```
alt + shift + f
```

Open the research :
```
Ctlr + shift + P 
```
