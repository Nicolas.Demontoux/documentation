# Getting started with GitLab

Link my project VisualStudioCode to git :
```
git clone <URL from github link copied >
```

Verify the modifications done in the project :
```
git status
```
Commit the project :
```
git add .
```
Select emojis about the modifications i have done :
```
gitmoji -c
```
Send all the modifications on my repository :
```
git push origin master 
```
### About the tags in GitLab

Retrieve the content in the server:
```
git fetch origin master
```
Retrieve the content of the server and merge with the local branch
```
git pull origin master
git tag -a 0.2.0
git push origin --tag
```
### Work in group with git:
Create a new branch :
```
git checkout -b "BranchName"
```
Switch to an other branch :
```
git checkout "BranchName"
```
### Link for the versionning/changelogs :
https://keepachangelog.com/fr/1.0.0/
https://semver.org/lang/fr/

### Resolve conflict :
