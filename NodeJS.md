## Getting started with node (Client)
Installation of the serve module: 
```
npm install -g serve
```
Start the project : 
```
serve
```
Go on the [here](http://localhost:5000)

## Getting started with node (Server)

###### Launch server nodejs :
```
node "name of my file"
```

###### Install package :
```
npm Install "name of the package"
```

## Getting started with express and nodeJS

###### Create new folder :
- mkdir "name of the folder"
then enter in the folder :
-cd "name of the folder"

###### Create de package.json :
```
npm init
```
###### Install express :
```
npm install express
```
###### Installing the express application generator :
```
npm install express-generator -g
```
To create an Express app named "helloworld" with the default settings :
```
express helloworld
```
Then run the app :
```
SET DEBUG=helloworld:* & npm start
```

###### Creating the project :

- express express-locallibrary-tutorial --view=pug

Install an automate restarting refresh instead of reload the server each time :
- For one project only : npm install --save-dev nodemon
- To install it globaly in the machine : npm install -g nodemon

###### REST API creation :

Start to install express and mongoose :
- npm install mongoose
- npm install express

To optimise execute files with nodemon
- nodemon 'nameOfFile.js'

CRUD : Create, Read, Update, Delete :

To create :
```
app.post(........
```
To read :
```
app.get(.......
```
To update :
```
app.patch|put(.....
```
To delete :
```
app.delete(.......
```