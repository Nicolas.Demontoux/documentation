# Getting started with JavaScript

### Axios :
Integrate axios : https://github.com/axios/axios

```
<script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
```

### things to know on JavaScript

Important thing to do on the first line :

``` 
document.addEventListener("DOMContentLoaded", async (event) => {
```
That will help the webpage to be loaded.
- the term "document" represent the html page.

About the addEventListener :
- First parameter is the type (click,cute,copy...)
- Second parameter is the listener
- Last one is named an option (facultative)